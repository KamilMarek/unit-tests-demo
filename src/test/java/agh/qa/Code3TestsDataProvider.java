package agh.qa;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Code3TestsDataProvider {
    // The same 3 tests but implemented in one Test method with DataProvider

    @DataProvider
    public Object[][] testDataProvider() {
        return new Object[][]{
                {1, 1, 0},
                {-1, -4, -7},
                {0, -3, 0}
        };
    }

    @Test(dataProvider = "testDataProvider")
    public void testUsingDataProvider(int a, int b, int expected){
        Code3 code3 = new Code3();
        int result = code3.calculate(a,b);

        Assert.assertEquals(result, expected, "Verify result " + a + ", " + b);
    }
}

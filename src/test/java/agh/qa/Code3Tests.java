package agh.qa;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Code3Tests {
    private Code3 _code3;

    @BeforeMethod
    public void SetUp(){
        _code3 = new Code3();
    }

    @Test
    public void Test1(){
        int result = _code3.calculate(1,1);

        Assert.assertEquals(result, 0, "Verify result 1");
    }

    @Test
    public void Test2(){
        int result = _code3.calculate(-1,-4);

        Assert.assertEquals(result, -7, "Verify result 2");
    }

    @Test
    public void Test3(){
        int result = _code3.calculate(0,-3);

        Assert.assertEquals(result, 0, "Verify result 3");
    }
}
